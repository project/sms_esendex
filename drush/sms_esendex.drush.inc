<?php
// $Id$

/**
 * @file
 * Drush integration for the sms_esendex module.
 */

/**
 * Implements hook_drush_command().
 */
function sms_esendex_drush_command() {
  $items['sms_esendex-download'] = array(
    'callback' => 'sms_esendex_drush_download',
    'description' => dt('Downloads the required esendex helper library from github.'),
    'arguments' => array(
      'path' => dt('Optional. A path to the download folder. If omitted Drush will use the default location (sites/all/libraries/esendex).'),
    ),
  );
  return $items;
}

/**
 * Downloads
 */
function sms_esendex_drush_download() {
  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/all/libraries/esendex';
  }
  if (drush_shell_exec('wget ' . SMS_ESENDEX_DOWNLOAD_ARCHIVE . ' && tar xvzf v' . SMS_ESENDEX_VERSION . '.tar.gz && rm v' . SMS_ESENDEX_VERSION . '.tar.gz && mv esendex-php-sdk-' . SMS_ESENDEX_VERSION . ' %s', $path)) {
    drush_log(dt('Esendex has been downloaded to @path.', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download the Esendex to @path.', array('@path' => $path)), 'error');
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_sms_esendex_post_enable() {
  $modules = func_get_args();
  if (in_array('sms_esendex', $modules)) {
    sms_esendex_drush_download();
  }
}
